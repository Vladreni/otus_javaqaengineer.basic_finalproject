package owner;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources("classpath:url.properties")
public interface OwnerConfig extends Config {

    @Key("url1")
    String urlOne();

    @Key("url2")
    String urlTwo();

    @Key("url3")
    String urlThree();



}
