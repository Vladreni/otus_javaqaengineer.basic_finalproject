package test;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.aeonbits.owner.ConfigFactory;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import owner.OwnerConfig;

import java.time.Duration;
import java.util.Set;

public class LoggerTest {
    private static org.apache.logging.log4j.Logger logger = LogManager.getLogger(LoggerTest.class);
    private OwnerConfig cfg = ConfigFactory.create(OwnerConfig.class);
    protected static WebDriver driver;

    @BeforeEach
    public void startUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        logger.info("Драйвер хром настроен");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        logger.info("Драйвер хром поднят");
    }

    @AfterEach
    public void end() {
        if (driver != null) {
            driver.quit();
            logger.info("Драйвер хром закрыт");
        }
    }

    @Test
    public void testOne() {
        driver.quit();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        logger.info("Драйвер хром headless поднят");

        driver.get(cfg.urlOne());
        logger.info("Сайт headless открыт");

        WebElement input = driver.findElement(By.id("search_form_input_homepage"));
        input.sendKeys("ОТУС\n");
        //input.sendKeys(Keys.ENTER);

        input = driver.findElement(By.cssSelector(".result__title.js-result-title"));
        Assertions.assertEquals(
                "Онлайн‑курсы для профессионалов, дистанционное обучение...", input.getText());
        logger.info("Тест завершен");
    }

    @Test
    public void testTwo() throws InterruptedException {
        driver.get(cfg.urlTwo());
        logger.info("Сайт открыт");

        driver.manage().window().fullscreen();
        logger.info("Сайт в киоске");

        WebElement input = driver.findElement(By.className("content-overlay"));
        input.click();
        input = input.findElement(By.xpath("//div[@class='content-overlay']/following-sibling::img"));

        WebElement inputModal = driver.findElement(By.id("fullResImage"));
        Assertions.assertEquals(input.getAttribute("src"), inputModal.getAttribute("src"));

        Thread.sleep(2000);

        logger.info("Тест завершен");
    }

    @Test
    public void testThree() {
        driver.get(cfg.urlThree());
        logger.info("Сайт открыт");

        driver.manage().window().maximize();
        logger.info("Сайт full screen");

        auth();

        Set<Cookie> input = driver.manage().getCookies();
        for (var i : input) {
            logger.info(i);
            //System.out.println(i.getName() + "\t\t" + i.getValue());
        }

        logger.info("Тест завершен");
    }

    private void auth() {
        String login = "vladreni@rambler.ru";
        String pass = "w2345678";
        String locator = "button.header2__auth.js-open-modal";
        driver.findElement(By.cssSelector(locator)).click();
        driver.findElement(By.cssSelector("div.new-input-line_slim:nth-child(3) > input:nth-child(1)")).sendKeys(login);
        driver.findElement(By.cssSelector(".js-psw-input")).sendKeys(pass);
        driver.findElement(By.cssSelector("div.new-input-line_last:nth-child(5) > button:nth-child(1)")).submit();
        logger.info("Авторизация успешна!");
    }


}
